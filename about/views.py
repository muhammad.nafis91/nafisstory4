from django.shortcuts import render

# Create your views here.
def index(request):
    context={
        "Title":"About",
        "Nama":"About",
        "NoHalaman":"2"
    }
    return render(request,'about/about.html',context)