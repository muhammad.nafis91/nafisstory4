from django.shortcuts import render

# Create your views here.
def index(request):
    context={
        "Title":"Home",
        "Nama":"Home",
        "NoHalaman":"1"
    }
    return render(request,'home/home.html',context)