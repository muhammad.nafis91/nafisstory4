from django.shortcuts import render

# Create your views here.
def index(request):
    context={
        "Title":"Blog",
        "Nama":"Blog",
        "NoHalaman":"3"
    }
    return render(request,'blog/blog.html',context)